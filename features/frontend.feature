Feature: LimeSurvey release monitor frontend
  As a LimeSurvey service administrator
  I want to be notified when a LimeSurvey security update is available

  Scenario: no new release
    Given the last checked tag was 2.71.0+170925
    And the most recent release is 2.71.0+170925
    And I have a local LimeSurvey Git clone
    When I run the LimeSurvey release monitor
    Then it should silently succeed
    And the last checked tag is 2.71.0+170925
    When I run the LimeSurvey release monitor
    Then it should silently succeed
    And the last checked tag is 2.71.0+170925

  Scenario: new release without security fix
    Given the last checked tag was 2.71.0+170925
    And the most recent release is 2.71.1+170927
    And I have a local LimeSurvey Git clone
    When I run the LimeSurvey release monitor
    Then it should succeed
    And I should be told "New release(s) available without security fixes"
    And I should be told "The latest release is 2.71.1"
    And the last checked tag is 2.71.1+170927
    When I run the LimeSurvey release monitor
    Then it should silently succeed
    And the last checked tag is 2.71.1+170927

  Scenario: latest new release has security fixes
    Given the last checked tag was 2.67.1+170626
    And the most recent release is 2.67.3+170728
    And I have a local LimeSurvey Git clone
    When I run the LimeSurvey release monitor
    Then it should succeed
    And I should be told "New release (2.67.3) fixes security issues"
    And I should be told "Users need to put in their old password to change the current"
    And I should be told "The latest release is 2.67.3"
    And the last checked tag is 2.67.3+170728
    When I run the LimeSurvey release monitor
    Then it should silently succeed
    And the last checked tag is 2.67.3+170728

  Scenario: intermediary new release has security fixes
    Given the last checked tag was 2.67.1+170626
    And the most recent release is 2.71.1+170927
    And I have a local LimeSurvey Git clone
    When I run the LimeSurvey release monitor
    Then it should succeed
    And I should be told "New release (2.67.3) fixes security issues"
    And I should be told "Users need to put in their old password to change the current"
    And I should be told "The latest release is 2.71.1"
    And the last checked tag is 2.71.1+170927
    When I run the LimeSurvey release monitor
    Then it should silently succeed
    And the last checked tag is 2.71.1+170927

  Scenario: new release without changelog entry
    Given the last checked tag was 2.67.3+170728
    And the most recent release is 2.71.1+170927
    And I have a local LimeSurvey Git clone whose release notes lack 2.71.1+170927
    When I run the LimeSurvey release monitor
    Then it should fail
    And I should be told "The last version in the release notes (2.71.0) is different from the newest tag (2.71.1+170927, version 2.71.1)"
    And the last checked tag is 2.67.3+170728

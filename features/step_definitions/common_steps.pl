use strictures 2;
use 5.10.1;

use lib qw{lib};
use Carp::Assert;
use Carp::Assert::More;
use Dpkg::Version;
use English qw{-no_match_vars};
use Env qw{@PATH};
use List::MoreUtils;
use Method::Signatures::Simple;
use Path::Tiny qw{path tempfile};
use Test::BDD::Cucumber::StepFile;
use Test::Git;
use Test::Most;

my $git_root_dir  = path(__FILE__)->parent->parent->parent;
my $bin_dir       = $git_root_dir->child('bin')->absolute;
my $test_data_dir = $git_root_dir->child('t', 'data')->absolute;
my $release_notes_snippets_dir = $test_data_dir->child('release_notes_snippets');
my $release_notes_header = $release_notes_snippets_dir->child('header')->slurp_utf8;

unshift @PATH, $bin_dir;

func set_up_mock_git_repo (%opts) {
    my $docs_dir = path($opts{repo}->work_tree)->child('docs');
    $docs_dir->mkpath;
    my $release_notes = $docs_dir->child('release_notes.txt');
    $release_notes->touch;
    my @versions = sort version_compare grep {
        my $tag = $_;
        List::MoreUtils::none { $_ eq $tag } ('old', 'header')
            and
        version_compare_relation($tag, REL_GE, $opts{last_checked_tag})
            and
        version_compare_relation($tag, REL_LE, $opts{newest_tag})
    } map {
        $_->basename
    } $release_notes_snippets_dir->children(qr{[+]});
    assert($versions[0]  eq $opts{last_checked_tag});
    assert($versions[-1] eq $opts{newest_tag});
    my $changes = '';
    for my $version (@versions) {
        if (! defined($opts{missing_changelog_entry})
                || $version ne $opts{missing_changelog_entry}) {
            warn "creating $version with changelog entry" if $ENV{DEBUG};
            $changes = $release_notes_snippets_dir->child($version)->slurp_utf8
                       . "\n\n"
                       . $changes;
            $release_notes->spew_utf8(
               $release_notes_header,
               "\n\n",
               $changes,
               "\n\n",
               $release_notes_snippets_dir->child('old')->slurp_utf8
            );
            $opts{repo}->run(add => "$release_notes");
            $opts{repo}->run(
                commit => '-m', "Release LimeSurvey $version",
                "$release_notes"
            );
        } else {
            warn "creating $version without changelog entry" if $ENV{DEBUG};
            my $dummy_file = path($opts{repo}->work_tree)->child($version);
            $dummy_file->touch;
            $opts{repo}->run(add => "$dummy_file");
            $opts{repo}->run(
                commit => '-m', "Release LimeSurvey $version",
                "$dummy_file"
            );
        }
        warn "tagging $version" if $ENV{DEBUG};
        $opts{repo}->run(
            tag => '-m', "Release LimeSurvey $version",
            $version
        );
    }
    say STDERR $release_notes->slurp_utf8 if $ENV{DEBUG};
}

Given qr{^I have a local LimeSurvey Git clone(?: (.*))?$}, func($c) {
    my $repo
        = $c->{stash}->{scenario}->{repo}
        = test_repository();
    my $missing_changelog_entry;
    if (defined $c->matches->[0]) {
        ($missing_changelog_entry)
            = ($c->matches->[0] =~ m{^whose release notes lack (.*)});
    }
    set_up_mock_git_repo(
        repo                    => $repo,
        last_checked_tag        => $c->{stash}->{scenario}->{last_checked_tag},
        newest_tag              => $c->{stash}->{scenario}->{newest_tag},
        missing_changelog_entry => $missing_changelog_entry,
    );
};

Given qr{^the last checked tag was (.*)$}, func($c) {
    my $last_checked_tag
        = $c->{stash}->{scenario}->{last_checked_tag}
        = $c->matches->[0];
    my $last_checked_cache_file
        = $c->{stash}->{scenario}->{last_checked_cache_file}
        = tempfile(UNLINK => 1);
    $last_checked_cache_file->spew_utf8($last_checked_tag);
    assert(-e $c->{stash}->{scenario}->{last_checked_cache_file});
};

Given qr{^the most recent release is (.*)$}, func($c) {
    $c->{stash}->{scenario}->{newest_tag} = $c->matches->[0];
};

When qr{^I run the LimeSurvey release monitor$}, func($c) {
    my $cmdline = sprintf(
        "%s %s --git_dir='%s' --last-checked-cache-file='%s'",
        path($bin_dir, 'tails-monitor-limesurvey-releases'),
        "--no-notification",
        $c->{stash}->{scenario}->{repo}->git_dir,
        $c->{stash}->{scenario}->{last_checked_cache_file},
    );
    warn $cmdline if $ENV{DEBUG};
    $c->{stash}->{scenario}->{output} = `$cmdline 2>&1`;
    $c->{stash}->{scenario}->{exit_code} = ${^CHILD_ERROR_NATIVE};
};

Then qr{^it should succeed$}, func($c) {
    if (defined $c->{stash}->{scenario}->{exit_code}
            && $c->{stash}->{scenario}->{exit_code} != 0
            && exists $c->{stash}->{scenario}->{output}
            && defined $c->{stash}->{scenario}->{output}) {
        warn $c->{stash}->{scenario}->{output};
    }

    ok(defined $c->{stash}->{scenario}->{exit_code})
        and
    is($c->{stash}->{scenario}->{exit_code}, 0);
};

Then qr{^it should silently succeed$}, func($c) {
    ok(defined $c->{stash}->{scenario}->{exit_code})
        and
    is($c->{stash}->{scenario}->{exit_code}, 0);
    ok(exists $c->{stash}->{scenario}->{output});
    ok(defined $c->{stash}->{scenario}->{output});
    is($c->{stash}->{scenario}->{output}, '');
};

Then qr{^it should fail$}, func($c) {
    ok(defined $c->{stash}->{scenario}->{exit_code})
        and
    isnt($c->{stash}->{scenario}->{exit_code}, 0);

    if ($c->{stash}->{scenario}->{exit_code} == 0) {
        warn $c->{stash}->{scenario}->{output};
    }
};

Then qr{^I should be told "([^"]+)"$}, func($c) {
    my $expected_output_re = quotemeta($c->matches->[0]);
    like($c->{stash}->{scenario}->{output}, qr{$expected_output_re});
};

Then qr{^the last checked tag is (.*)$}, func($c) {
    is(
        $c->{stash}->{scenario}->{last_checked_cache_file}->slurp_utf8,
        $c->matches->[0]
    );
};

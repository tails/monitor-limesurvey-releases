package Tails::LimeSurvey::ReleaseNotesParser;

use strictures 2;
use autodie;
use 5.10.1;

use Exporter;
our @ISA = qw{Exporter};
our @EXPORT_OK = qw{
    has_security_fixes
    new_releases
};

use Dpkg::Version;
use Method::Signatures::Simple;

func has_security_fixes ($release_metadata) {
    exists $release_metadata->{security_fixes}
        && defined $release_metadata->{security_fixes}
        && ref($release_metadata->{security_fixes}) eq 'ARRAY'
        && scalar(@{$release_metadata->{security_fixes}});
}

# First element of return list is undef if no next changelog entry is found
func extract_next_changelog_entry ($changelog) {
    my $changelog_entry;
    # Changes from 2.67.0 (build 170622) to 2.67.1 (build 170626) Jun 26, 2017
    # But the "build YYMMDD" info is sometimes wrong, so we ignore it.
    my ($to_version, $to_date, $changes, $remaining_changelog)
        = ($changelog =~ m{
               \A
               .*?
               Changes
               \s+
               from
               \s+
               [0-9.]+
               \s+
               [(] build \s+ \d{6} [)]
               \s+
               to
               \s+
               ([0-9.]+)
               \s+
               [(] build \s+ (\d{6}) [)]
               [^\n]*
               \n
               (.*?)
               (Changes
               \s+
               from
               \s+
               [0-9.]+
               \s+
               [(] build \s+ \d{6} [)]
               \s+
               to
               \s+
               [0-9.]+
               \s+
               [(] build \s+ \d{6} [)]
               .*
               )
           }xms);
    if (defined($to_version) and defined($to_date)) {
        $changelog_entry = {
            version => "${to_version}",
            changes => $changes,
        };
    }
    return ($changelog_entry, $remaining_changelog);
}

func security_fixes ($changes) {
    map {
        my $change = $_;
        $change =~ s{\n\z}{}xms;
        $change;
    } grep {/security/i} split(/\n/, $changes);
}

func new_releases (%opts) {
    my $since_version = $opts{since_version};
    my $until_version = $opts{until_version};
    my $release_notes = $opts{release_notes};

    my $changelog_header =
        q{CHANGE LOG} . "\n"
        . q{------------------------------------------------------};
    $release_notes =~ s{
        \A
        .*
        $changelog_header
    }{}xms;

    my @new_releases;
    while (1) {
        my $changelog_entry;
        ($changelog_entry, $release_notes)
            = extract_next_changelog_entry($release_notes);
        last unless defined $changelog_entry;
        if (defined $until_version) {
            next if version_compare_relation(
                $changelog_entry->{version},
                REL_GT,
                $until_version
            );
        }
        last if $changelog_entry->{version} eq $since_version;
        push @new_releases, {
            version        => $changelog_entry->{version},
            security_fixes => [security_fixes($changelog_entry->{changes})],
        };
    }
    return @new_releases;
}

1;

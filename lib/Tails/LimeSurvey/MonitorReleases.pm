=head1 NAME

Tails::LimeSurvey::MonitorReleases - monitor LimeSurvey releases

=head1 LICENSE

Copyright (C) 2017 Tails developers <tails@boum.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

package Tails::LimeSurvey::MonitorReleases;

use strictures 2;
use autodie;
use 5.10.1;

our $VERSION = '0.1';

use Carp;
use Dpkg::Version;
use Git::Repository;
use Method::Signatures::Simple;
use Moo;
use MooX::Options;
use Path::Tiny;
use Tails::LimeSurvey::ReleaseNotesParser qw{has_security_fixes new_releases};
use Try::Tiny;
use Types::Standard qw{:all};

option 'debug' => (
    doc     => q{Report additional info to STDERR},
    isa     => Bool,
    is      => 'ro',
    short   => 'd',
    default => sub { $ENV{DEBUG} },
);

option 'notification' => (
    doc         => q{Notify the service administrators},
    isa         => Bool,
    negativable => 1,
    is          => 'ro',
    default     => sub { 0 },
);

option 'last_checked_cache_file' => (
    doc      => q{File containing the last tag that has been checked},
    required => 1,
    is       => 'ro',
    format   => 's',
    isa      => InstanceOf['Path::Tiny'],
    coerce   => sub { path($_[0]) },
);

option 'git_dir' => (
    doc      => q{Path to a local LimeSurvey Git repository's $GIT_DIR},
    required => 1,
    is       => 'ro',
    format   => 's',
);

has 'repo' => (
    is      => 'lazy',
    isa     => InstanceOf['Git::Repository'],
    default => sub {
        my $self = shift;
        Git::Repository->new(git_dir => $self->git_dir),
    },
);

has 'last_checked_tag' => (
    is      => 'lazy',
    isa     => Maybe[Str],
    default => sub {
        my $self = shift;
        if (-e $self->last_checked_cache_file) {
            my $tag = $self->last_checked_cache_file->slurp_utf8;
            chomp $tag;
            $tag;
        } else {
            $self->print_debug(
                "Last checked cache file ("
                . $self->last_checked_cache_file
                . ") is not readable."
            );
            undef;
        }
    },
);

has 'last_checked_version' => (
    doc => q{Last checked version without the +YYMMDD suffix},
    is      => 'lazy',
    isa     => Maybe[Str],
    default => sub {
        my $self = shift;
        if (defined $self->last_checked_tag) {
            $self->print_debug(
                "Last checked tag: ",
                $self->last_checked_tag,
            );
            my ($version) = ($self->last_checked_tag =~ m{\A(.*)[+]\d+\z}xms);
            $version;
        } else {
            undef;
        }
    },
);

method print (@strings) {
    return unless @strings;
    return unless grep {length($_)} @strings;
    say STDOUT @strings;
}

method print_debug (@strings) {
    return unless $self->debug;
    return unless @strings;
    return unless grep {length($_)} @strings;
    say STDERR @strings;
}

func max_version (@versions) {
    @versions = sort { version_compare($a, $b) } @versions;
    return $versions[-1];
}

func report (@new_releases) {
    return '' unless @new_releases;
    my $report = '';
    if (grep { has_security_fixes($_) } @new_releases) {
        $report .=
            join(
                "\n",
                map {
                    my $version = $_->{version};
                    "New release ($version) fixes security issues:\n"
                    . join(
                        "\n",
                        map { " - $_" } @{$_->{security_fixes}}
                    )
                } grep { has_security_fixes($_) } @new_releases
            );
    } else {
        $report .= "New release(s) available without security fixes";
    }
    $report .= "\n";
    $report .= "The latest release is "
        . max_version(map { $_->{version} } @new_releases);
}

method update_repo () {
    $self->repo->run('remote') || return;
    my @git_options = $self->debug ? '--verbose' : '--quiet';
    if ($self->repo->work_tree) {
        $self->repo->run('pull' => @git_options);
    } else {
        $self->repo->run('fetch' => @git_options, '--tags');
    }
}

method run {
    $self->update_repo;
    my $newest_tag = $self->repo->run(
        'describe' => '--abbrev=0', '--tags', 'master'
    );
    $self->print_debug("Newest tag: $newest_tag");
    my ($newest_tagged_version) = ($newest_tag =~ m{\A(.*)[+]\d+\z}xms);
    $self->print_debug("Last tagged version: ", $newest_tagged_version);
    if (defined $self->last_checked_version) {
        my $release_notes = $self->repo->run(
            'show' => "master:docs/release_notes.txt"
        );
        $self->print_debug(
            "Last checked version: ",
            $self->last_checked_version
        );
        my @new_releases = new_releases(
            since_version => $self->last_checked_version,
            until_version => $newest_tagged_version,
            release_notes => $release_notes,
        );
        if (version_compare_relation($newest_tag, REL_GT, $self->last_checked_tag)) {
            croak("Latest release ($newest_tag) not found in release notes")
                unless @new_releases;
            my $last_release_in_changelog
                = max_version(map { $_->{version} } @new_releases);
            $self->print_debug("Last release in changelog: $last_release_in_changelog");
            if (! version_compare_relation(
                $last_release_in_changelog, REL_EQ, $newest_tagged_version
            )) {
                croak(sprintf(
                    "The last version in the release notes (%s) "
                        . "is different from the newest tag (%s, version %s)",
                    $last_release_in_changelog, $newest_tag, $newest_tagged_version
                ));
            }
        }
        $self->print(report(@new_releases));
    }
    $self->last_checked_cache_file->spew_utf8($newest_tag);
}

1;

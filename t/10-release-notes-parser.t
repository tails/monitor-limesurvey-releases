use Test::Spec;

use strictures 2;
use 5.10.1;

use Dpkg::Version;
use Method::Signatures::Simple;
use Path::Tiny qw{cwd path};
use Tails::LimeSurvey::ReleaseNotesParser qw{new_releases};

my $test_data_dir = path(__FILE__)->parent->child('data')->absolute;
my $release_notes_snippets_dir = $test_data_dir->child('release_notes_snippets');
my $release_notes_header = $release_notes_snippets_dir->child('header')->slurp_utf8;

func generate_release_notes (@versions) {
    my $release_notes = '';
    for my $version (sort version_compare @versions) {
        $release_notes =
            $release_notes_snippets_dir->child($version)->slurp_utf8
            . "\n\n"
            . $release_notes;
    }
    $release_notes =
        $release_notes_header
        . "\n\n"
        . $release_notes
        . "\n\n"
        . $release_notes_snippets_dir->child('old')->slurp_utf8;
    say STDERR $release_notes if $ENV{DEBUG};
    return $release_notes;
}

describe 'The new_releases function' => sub {
    describe 'when run on release notes that contain only the "since" & "until" versions' => sub {
        my $since_version = '2.67.1';
        my $until_version = '2.67.3';
        my @new_releases = new_releases(
            since_version => $since_version,
            release_notes => generate_release_notes($since_version, $until_version),
        );
        it 'returns only the "until" version' => sub {
            is(scalar(@new_releases), 1);
            is($new_releases[0]->{version}, $until_version);
        };
    };
    describe 'when run on release notes that contain an unreleased version' => sub {
        my $old_version = '2.67.1';
        my $last_released_version = '2.67.3';
        my $last_entry_in_release_notes = '2.71.0';
        my @new_releases = new_releases(
            since_version => $old_version,
            until_version => $last_released_version,
            release_notes => generate_release_notes(
                $old_version,
                $last_released_version,
                $last_entry_in_release_notes),
        );
        it 'returns only the last released version' => sub {
            is(scalar(@new_releases), 1);
            is($new_releases[0]->{version}, $last_released_version);
        };
    };
};

runtests unless caller;

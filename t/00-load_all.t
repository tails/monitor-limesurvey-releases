use Test::Most;

use Path::Class;
use Module::Pluggable::Object;

# libs
my @needsX;
my $finder = Module::Pluggable::Object->new(
    search_path => [ 'Tails' ],
);
foreach my $class (grep !/\.ToDo/,
                   sort do { local @INC = ('lib'); $finder->plugins }) {
  use_ok($class);
}

done_testing();
